# Подсчет количества букв
from collections import Counter


def col_let(st):

    c = Counter(st)
    col = ''
    for i, j in c.items():
        if j > 1:
            col += i
            col += str(j)
        elif j == 1:
            col += i

    return col


col_let("cccbba")
print(col_let("cccbba"))
col_let("abeehhhhhccced")
print(col_let("abeehhhhhccced"))
col_let("aaabbceedd")
print(col_let("aaabbceedd"))
col_let("abcde")
print(col_let("abcde"))
