# Простейший калькулятор v0.1


# Простейший калькулятор v0.1

def calc():
    """
      Select an operation:
      1. Addition
      2. Subtraction
      3. Multiplication
      4. Division
    """
    print(calc.__doc__)


def sy():
    add = nu_1 + nu_2
    ad = str(add)
    t = ad.index('.')
    print(f'Private: {int(add)}, after: {ad[t + 1]}')


def mi():
    sub = nu_1 - nu_2
    su = str(sub)
    k = su.index('.')
    print(f'Private: {int(sub)}, after: {su[k + 1]}')


def ym():
    mul = nu_1 * nu_2
    mu = str(mul)
    f = mu.index('.')
    print(f'Private: {int(mul)}, after: {mu[f + 1]}')


def de():
    if nu_2 == 0:
        print('Division by zero is not allowed')

    else:
        div = nu_1 / nu_2
        di = str(div)
        s = di.index('.')
        print(f'Private: {int(div)}, after: {di[s + 1]}')


while True:
    st_n = int(input('Enter the menu item number: '))

    if st_n < 1 or st_n > 4:
        print('Error in the operation number!!!')
        break

    nu_1 = float(input('Enter the first number: '))
    nu_2 = float(input('Enter the second number: '))

    if st_n == 1:
        sy()
    elif st_n == 2:
        mi()
    elif st_n == 3:
        ym()
    elif st_n == 4:
        de()
