"""Задание 2
Открыть сайт http://demo.guru99.com/test/newtours/register.php
Заполнить все поля
Нажать кнопку Submit
Проверить, что отображается правильно имя и фамилия
Подсказка xpath ".//tr//table//font[3]”
Проверить, что отображается правильно username
Подсказка xpath ".//tr//table//font[5]”"""

v1 = "Julia"
v2 = "Gahovich"
v3 = "Julia_G"


def test_form(browser):
    browser.get("http://demo.guru99.com/test/newtours/register.php")
    name = browser.find_element_by_xpath("//input[@name='firstName']")
    name.send_keys(v1)
    last_name = browser.find_element_by_xpath("//input[@name='lastName']")
    last_name.send_keys(v2)
    phone = browser.find_element_by_xpath("//input[@name='phone']")
    phone.send_keys("+375298259568")
    email = browser.find_element_by_xpath("//input[@id='userName']")
    email.send_keys("Ty4kayes@yandex.ru")
    address = browser.find_element_by_xpath("//input[@name='address1']")
    address.send_keys("Tanka, 6")
    city = browser.find_element_by_xpath("//input[@name='city']")
    city.send_keys("Minsk")
    state = browser.find_element_by_xpath("//input[@name='state']")
    state.send_keys("Minsk")
    post = browser.find_element_by_xpath("//input[@name='postalCode']")
    post.send_keys("220004")
    browser.find_element_by_xpath(
        "//select[@name='country']/option[text()='BELARUS']").click()
    user = browser.find_element_by_xpath("//input[@name='email']")
    user.send_keys(v3)
    pswd = browser.find_element_by_xpath("//input[@name='password']")
    pswd.send_keys("qwerty123")
    cpswd = browser.find_element_by_xpath("//input[@name='confirmPassword']")
    cpswd.send_keys("qwerty123")
    btn = browser.find_element_by_xpath("//input[@name='submit']")
    btn.click()

    msg1 = browser.find_element_by_xpath(
        "//b[contains(text(),'Dear')]")
    msg2 = browser.find_element_by_xpath(
        "//b[contains(text(),'Your user name')]")

    assert msg1.text == f'Dear {v1} {v2},'
    assert msg2.text == f'Note: Your user name is {v3}.'
