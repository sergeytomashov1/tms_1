"""Задание 1:
Залогинется созданным пользователем:
1. Открыйть сайт http://thedemosite.co.uk/login.php
2. Ввести имя в поле username
3. Ввести пароль в поле password
4. Нажать на кнопку Test Login
5. Проверить, что Successful Login отображаются
"""


def test_add_user(browser):
    browser.get("http://thedemosite.co.uk/addauser.php")
    name = browser.find_element_by_xpath("//input[@name='username']")
    name.send_keys("Julia")
    pswd = browser.find_element_by_xpath("//input[@name='password']")
    pswd.send_keys("qwerty123")
    btn = browser.find_element_by_xpath("//input[@type='button']")
    btn.click()


def test_login(browser):
    browser.get("http://thedemosite.co.uk/login.php")
    name = browser.find_element_by_xpath("//input[@name='username']")
    name.send_keys("Julia")
    pswd = browser.find_element_by_xpath("//input[@name='password']")
    pswd.send_keys("qwerty123")
    btn = browser.find_element_by_xpath("//input[@type='button']")
    btn.click()
    ms = browser.find_element_by_xpath(
        "//big/blockquote/blockquote/font/center/b")

    assert ms.text == "**Successful Login**"
