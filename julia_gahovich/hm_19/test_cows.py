import pytest
import numpy as np
from bulls_cows import bulls_cows, computer_choice


class TestBullsCows:
    computer_number = computer_choice()
    bul_int = computer_number[::-1]
    warn = 'Введите комбинацию из четырех неповторяющихся цифр'

    @pytest.mark.smoke
    def test_win(self):
        assert bulls_cows(self.computer_number, self.computer_number) \
            == 'Вы выиграли!', f'Не верная {self.computer_number}' \
            f'победная комбинация'

    @pytest.mark.smoke
    def test_bul(self):
        bul_comb = bulls_cows(self.bul_int, self.computer_number)
        assert ''.join(bul_comb) \
            == 'Четыре коровы,ноль быков', f'Не верное {self.bul_int} ' \
            f'количество коров'

    @pytest.mark.smoke
    def test_cow(self):
        rotate_combination = ''.join(np.roll(list(self.computer_number), -1))
        cow_comb = bulls_cows(rotate_combination, self.computer_number)
        assert ' '.join(cow_comb) \
            == 'Четыре коровы, ноль быков', f'Не верное {rotate_combination}' \
            f'количество быков'

    @pytest.mark.smoke
    @pytest.mark.parametrize("params", ['123', '1111', 'ab!@'])
    def test_negative(self, params):
        assert bulls_cows(params, self.computer_number) \
            == 'Введите комбинацию из четырех неповторяющихся цифр',\
            f'Не корректная работа с негативными {params} параметрами'

    @pytest.mark.xfail(reason="fixing this bug right now")
    def test_negative_empty(self):
        assert bulls_cows('', self.computer_number) == 'Вы выиграли!',\
            'Не корректная работа с пустым значением'
