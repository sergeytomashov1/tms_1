import pytest
from count import counter


class TestCount:

    @pytest.mark.smoke
    @pytest.mark.linux
    def test_positive_abc(self):
        assert counter('ass  saa') == 'as2 2sa2',\
            f'Не верный подсчет букв, {counter("ass  saa")} не равно as2 2sa2'

    @pytest.mark.smoke
    @pytest.mark.linux
    def test_positive_fig(self):
        assert counter('1233') == '1232',\
            f'Не верный подсчет цифр, {counter("1233")} не равно 1232'

    @pytest.mark.smoke
    @pytest.mark.linux
    def test_positive_none(self):
        assert counter('') == '', 'Не корректное работа с пустым значением'

    @pytest.mark.skip('Test is out of dates')
    def test_positive_symb(self):
        assert counter('!@#!@##') == '!@#!@#2', f'Не верный подсчет,' \
            f'{counter("!@#!@##")} не равно !@#!@#2'

    @pytest.mark.smoke
    @pytest.mark.linux
    @pytest.mark.xfail('Will be fixed soon')
    def test_negative(self):
        assert counter('1234') == '11213141', f'Не верный подсчет цифр,' \
            f'{counter("1234")} не равно 11213141'
