"""Напишите функцию, которая принимает на вход одномерный массив и два числа
- размеры выходной матрицы. На выход программа должна подавать матрицу нужного
размера, сконструированную из элементов массива.
reshape([1, 2, 3, 4, 5, 6], 2, 3) =>
[
    [1, 2, 3],
    [4, 5, 6]
]
reshape([1, 2, 3, 4, 5, 6, 7, 8,], 4, 2) =>
[
    [1, 2],
    [3, 4],
    [5, 6],
    [7, 8]
]
"""


def reshape(input_arr, rows, cols):
    rows_arr = list()
    n = 0

    for i in range(0, rows):
        column_arr = list()

        for j in range(0, cols):
            column_arr.append(input_arr[n])
            n += 1
        rows_arr.append(column_arr)

    return rows_arr


x = reshape([1, 2, 3, 4, 5, 6, 7, 8, ], 4, 2)

for i, row in enumerate(x):
    if i == len(x) - 1:
        print(row, end='\n')
    else:
        print(row, end=',\n')

# Вариант 2
# import numpy as np
#
#
# def reshape_1(a, b, c):
#     a = np.array(a)
#     x = a.reshape(b, c)
#     print(x)
#
#
# reshape_1([1, 2, 3, 4, 5, 6, 7, 8,], 4, 2)
