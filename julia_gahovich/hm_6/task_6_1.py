"""Ваша задача написать программу, принимающее число - номер кредитной карты
(число может быть четным или не четным). И проверяющей может ли такая карта
существовать. Предусмотреть защиту от ввода букв, пустой строки и т.д.
Cards for test:
378282246310005
371449635398431
378734493671000
5610591081018250
30569309025904
38520000023237
6011111111111117
6011000990139424
3530111333300000
4222222222222
"""


def card_validator():
    while True:
        given_card = input('Введите номер карты: ')
        try:
            if len(given_card) <= 16:
                digits_sum = 0
                num_digits = len(given_card)
                # print(num_digits)
                odd_even = num_digits % 2
                for count in range(0, num_digits):
                    digit = int(given_card[count])
                    if not ((not count % 2 and odd_even) or (
                            count % 2 and not odd_even)):
                        digit = digit * 2
                    if digit > 9:
                        digit = digit - 9
                    digits_sum = digits_sum + digit
                if digits_sum % 10 == 0:
                    print(f'Карта {given_card} успешно прошла проверку')
                else:
                    print(f'Карта {given_card} не прошла проверку')
                break
            else:
                print(f'Карта {given_card} не прошла проверку')
                break
        except ValueError:
            print('Вы ввели некорректное значение! Попробуйте еще раз')


card_validator()
