"""работа с json файлом
Разработайте поиск учащихся в одном классе, посещающих одну секцию.
Фильтрацию учащихся по их полу. Поиск ученика по имени(часть имени)

Файл: students.json
"""

import json


def json_parse():
    with open('students.json') as f:
        json_data = json.load(f)
        return json_data


def same_class_section(value1, value2):
    student_list = []
    student_dict = json_parse()
    for i in student_dict:
        if i['Class'] == value1 and i['Club'] == value2:
            student_list.append(i['ID'])
    print(student_list)


def gender_filter(gender):
    gender_list = []
    student_dict = json_parse()
    for i in student_dict:
        if i['Gender'] == gender:
            gender_list.append(i['ID'])
    print(gender_list)


def search_by_name(name):
    student_dict = json_parse()
    for i in student_dict:
        if name in i['Name']:
            print(i['ID'])


json_parse()
same_class_section('3a', 'Chess')
gender_filter('W')
search_by_name('Saki')
