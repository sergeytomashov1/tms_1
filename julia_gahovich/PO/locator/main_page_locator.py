from selenium.webdriver.common.by import By


class MainPageLocator:

    LOCATOR_SIGN_IN_BUTTON = (By.CLASS_NAME, 'login')
    LOCATOR_CONTACT_US_FORM = (By.ID, 'contact-link')
    LOCATOR_CART = (By.XPATH, '//div[@class="shopping_cart"]/a')
    LOCATOR_PRODUCT_IMAGE = (By.XPATH, '//a[@class="product-name"]')
    LOCATOR_SUBMIT_BUTTON = (By.XPATH, '//*[@name="Submit"]')
    LOCATOR_CROSS_BUTTON = (By.XPATH, '//*[@class="cross"]')
