from selenium.webdriver.common.by import By


class ContactUsLocator:

    LOCATOR_CONTACT_US_FORM = (By.CLASS_NAME, 'contact-form-box')
    LOCATOR_CONTACT_US_TEXT = (By.XPATH,
                               '//*[@id="center_column"]/form/fieldset/h3')
    LOCATOR_SUBJECT_HEADING = (By.ID, 'id_contact')
    LOCATOR_EMAIL_ADDRESS = (By.ID, 'email')
    LOCATOR_ORDER_REFERENCE = (By.ID, 'id_order')
    LOCATOR_ATTACHED_FILE = (By.ID, 'fileUpload')
    LOCATOR_MESSAGE = (By.ID, 'message')
    LOCATOR_SEND_BUTTON = (By.ID, 'submitMessage')
    LOCATOR_SUCCESS_MSG = (By.XPATH, '//*[@id="center_column"]/p')
