import pytest
from .count_str import count_letters


@pytest.mark.usefixtures('fixture_class_start', 'fixture_start_test')
@pytest.mark.positive
class TestStrPositive:

    def test_1(self):
        assert count_letters('cccbba') == 'c3b2a'

    def test_2(self):
        assert count_letters('abeehhhhhccced') == 'abe3h5c3d'

    def test_3(self):
        assert count_letters('аабббвввв') == 'а2б3в4'

    def test_4(self):
        assert count_letters('abcde') == 'abcde'

    def test_5(self):
        assert count_letters('ABBCCCDDDD') == 'AB2C3D4'

    def test_6(self):
        assert count_letters('aaaaaaaaaaaa') == 'a12'


@pytest.mark.usefixtures('fixture_class_start', 'fixture_start_test')
@pytest.mark.xfail(reason='We fixing this right now!')
@pytest.mark.negative
class TestStrNegative:

    def test_1(self):
        assert count_letters('1111') == 'Проверьте правильность ввода'

    def test_2(self):
        assert count_letters('') == 'Проверьте правильность ввода'

    def test_3(self):
        assert count_letters(',./?;') == 'Проверьте правильность ввода'

    def test_4(self):
        assert count_letters('a1b2') == 'Проверьте правильность ввода'

    def test_5(self):
        assert count_letters('a  d') == 'Проверьте правильность ввода'
