import pytest
from .bulls_cows import ranch


@pytest.mark.usefixtures('fixture_class_start', 'fixture_start_test')
@pytest.mark.positive
class TestRanchPositive:

    def test_1(self, random_n):
        assert ranch(random_n, random_n) == f'Вы отгадали число, это было ' \
                                            f'{random_n}'

    def test_2(self, random_n):
        assert ranch(random_n, random_n[::-1]) == '0 bull and 4 cows!'

    def test_3(self):
        assert ranch('1234', '1235') == '3 bulls and 0 cow!'

    def test_4(self):
        assert ranch('1234', '1562') == '1 bull and 1 cow!'

    def test_5(self):
        assert ranch('1234', '3456') == '0 bull and 2 cows!'

    def test_6(self):
        assert ranch('1234', '1243') == '2 bulls and 2 cows!'


@pytest.mark.usefixtures('fixture_class_start', 'fixture_start_test')
@pytest.mark.negative
@pytest.mark.xfail(reason='It is bag and we are fixing this right now!')
class TestRanchNegative:

    def test_1(self, random_n):
        assert ranch(random_n, '') == 'Размер введенного числа не равен 4'

    def test_2(self, random_n):
        assert ranch(random_n, '....') == 'Вы ввели не число'

    def test_3(self, random_n):
        assert ranch(random_n, 'qwer') == 'Вы ввели не число'

    def test_4(self):
        assert ranch('qwer', '1234') == 'Загаданное значение не явл числом'

    def test_5(self, random_n):
        assert ranch(random_n, '1111') == 'Вы передали число с одинаковыми ' \
                                          'цифрами'
