from selenium.webdriver.common.by import By


def test_xpath_selector(browser):
    """кнопка по xpath"""

    browser.get("https://ultimateqa.com/complicated-page/")
    xpath = '//a[@class="et_pb_button et_pb_button_4 et_pb_bg_layout_light"]'
    button = browser.find_element_by_xpath(xpath)
    button.click()


def test_css_selector(browser):
    """Кнопка по css selector"""

    browser.get("https://ultimateqa.com/complicated-page/")
    css_selector = '.et_pb_button.et_pb_button_4'
    button = browser.find_element(By.CSS_SELECTOR, css_selector)
    button.click()


def test_class_name(browser):
    """Кнопка по class name"""

    browser.get("https://ultimateqa.com/complicated-page/")
    class_name = "et_pb_button_4"
    button = browser.find_element(By.CLASS_NAME, class_name)
    button.click()
