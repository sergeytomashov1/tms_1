from selenium.webdriver.common.by import By


def test_1(browser):
    """Заполняем все поля и проверяем вывод"""

    browser.get("https://ultimateqa.com/filling-out-forms/")
    name = "Maks"
    message = "Hello World!"

    name_xpath = '//input[@id="et_pb_contact_name_0"]'
    browser.find_element(By.XPATH, name_xpath).send_keys(name)

    message_xpath = '//textarea[@id="et_pb_contact_message_0"]'
    browser.find_element(By.XPATH, message_xpath).send_keys(message)

    button_xpath = '//div[@id="et_pb_contact_form_0"]//button'
    browser.find_element(By.XPATH, button_xpath).click()

    text_xpath = '//p[@class="et_pb_contact_error_text"]'
    text_field = browser.find_element(By.XPATH, text_xpath)
    assert text_field.text == "Form filled out successfully"


def test_2(browser):
    """Заполняем только поле Name и проверяем вывод"""

    browser.get("https://ultimateqa.com/filling-out-forms/")
    name = "Maks"

    name_xpath = '//input[@id="et_pb_contact_name_0"]'
    browser.find_element(By.XPATH, name_xpath).send_keys(name)

    button_xpath = '//div[@id="et_pb_contact_form_0"]//button'
    browser.find_element(By.XPATH, button_xpath).click()

    text_xpath = '//div[@class="et-pb-contact-message"]'
    text_field = browser.find_element(By.XPATH, text_xpath)
    assert text_field.text == "Please, fill in the following fields:\nMessage"


def test_3(browser):
    """Заполняем только поле Message и проверяем вывод"""

    browser.get("https://ultimateqa.com/filling-out-forms/")
    message = "Hello World!"

    message_xpath = '//textarea[@id="et_pb_contact_message_0"]'
    browser.find_element(By.XPATH, message_xpath).send_keys(message)

    button_xpath = '//div[@id="et_pb_contact_form_0"]//button'
    browser.find_element(By.XPATH, button_xpath).click()

    text_xpath = '//div[@class="et-pb-contact-message"]'
    text_field = browser.find_element(By.XPATH, text_xpath)
    assert text_field.text == "Please, fill in the following fields:\nName"
