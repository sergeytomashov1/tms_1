"""Библиотека"""


class Book:
    """Создание книги, True - книга доступна, False - книга занята"""

    def __init__(self, book_name, book_author, number_pages, isbn, flag):
        self._book_name = book_name
        self._book_author = book_author
        self._number_pages = number_pages
        self._isbn = isbn
        self.flag = flag

    def book_name(self):
        """Возвращает название и автора книги"""

        book_name = f"{self._book_name}, автор {self._book_author}"
        return book_name

    def check_book(self):
        """Проверка наличия книги"""

        return self.flag

    def take_book(self):
        """Книгу взяли"""

        self.flag = False
        return self.flag

    def return_book(self):
        """Книгу вернули"""

        self.flag = True
        return self.flag


class User:
    """Создание пользователя"""

    def __init__(self, user_name):
        self.user_name = user_name

    def give_book_to_user(self, book):
        """Выдает книгу пользователю и записывает ее на него"""

        print(f"{self.user_name}, Вы хотите взять книгу {book.book_name()}")
        check_book = book.check_book()
        if check_book is True:
            print(f"{self.user_name}, Вы взяли эту книгу, "
                  f"не забудьте ее вернуть")
            book.take_book()
        elif check_book is False:
            print("К сожалению эту книгу читает кто-то другой")

    def return_book_to_library(self, book):
        """Возвращает книгу и списывает ее с пользователя"""

        print(f"Вы хотите вернуть книгу {book.book_name()}")
        book.return_book()
        print(f"{self.user_name} вернул книгу, которую брал")


book_1 = Book("1984", "Джорж Оруэлл", 328, "0-1234567-8-9", True)
book_2 = Book("Бойцовский клуб", "Чак Паланик", 255, "978-5-17-107550-7",
              True)

user_1 = User("Max")
user_2 = User("Dzmitry")

user_1.give_book_to_user(book_2)
user_2.give_book_to_user(book_1)
user_1.return_book_to_library(book_2)
user_1.give_book_to_user(book_1)
