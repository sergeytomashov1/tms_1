"""Цветочница"""


class Flower:
    """
    Создание цветка по праметрам имя, время увядания в днях, цвет, длина
    стебля в см, цена в руб
    """

    def __init__(self, name, wilting, color, stem_length, cost):
        self.name = name
        self.wilting = wilting
        self.color = color
        self.stem_length = stem_length
        self.cost = cost


class Accessory:
    """
    Создание аксессуара по параметрам имя, описание, цена в руб
    """

    def __init__(self, name, description, cost):
        self.name = name
        self.description = description
        self.cost = cost


class Bouquet:
    """Создание букета"""

    def __init__(self):
        self.bouquet = []
        self.accessories = []

    def add_flower(self, *flowers):
        """Добавляет цветок в букет"""

        for flower in flowers:
            self.bouquet.append(flower)

    def add_accessory(self, *accessories):
        """Добавляет аксессуар в букет"""

        for accessory in accessories:
            self.accessories.append(accessory)

    def total_cost(self):
        """Считает и выводит итоговую стоимость всего букета"""

        cost = 0
        for flower in self.bouquet:
            cost += flower.cost
        for thing in self.accessories:
            cost += thing.cost
        print(f"Цена всего букета составляет {cost} рублей")

    def time_wilting(self):
        """Выводит время увядание по среднему значению жизни каждого цветка"""

        time = 0
        len_bouquet = len(self.bouquet)
        for flower in self.bouquet:
            time += flower.wilting
        print(f"Время увядания равно {int(time/len_bouquet)} дней")

    def sort_by_parameter(self, parameter):
        """Сортирует букет по увяданию в порядке возрастания"""

        flower_list = {}
        flower_parameter = []
        flower_names = []
        for flower in self.bouquet:
            if parameter == "wilting":
                by_parameter = flower.wilting
            elif parameter == "cost":
                by_parameter = flower.cost
            elif parameter == "length":
                by_parameter = flower.stem_length
            flower_list[flower.name] = by_parameter
            flower_parameter.append(by_parameter)
        flower_parameter.sort()
        for n in flower_parameter:
            for key, value in flower_list.items():
                if n == value:
                    flower_names.append(key)
        print(f"Сортировка по параметру '{parameter}' в порядке возрастания: "
              f"{', '.join(flower_names)}")

    def find_by_parameter(self, parameter, parameter_value):
        """Поиск по параметрам"""

        for flower in self.bouquet:
            if parameter in flower.__dict__:
                if flower.__dict__[parameter] == parameter_value:
                    print(f"По параметру {parameter} со значением "
                          f"{parameter_value} в букете был найден цветок "
                          f"'{flower.__dict__['name']}'")

    def if_flower_in(self, flower_name):
        """Есть ли цветок в букете"""

        flowers_names = []
        for flower in self.bouquet:
            flowers_names.append(flower.name)
        if flower_name.title() in flowers_names:
            print(f"Цветок '{flower_name}' есть в букете")
        else:
            print(f"Цветка '{flower_name}' нет в букете")


rose = Flower('Роза', 6, 'красный', 60, 5)
tulip = Flower('Тюльпан', 5, 'желтый', 35, 4)
chamomile = Flower('Ромашка', 8, 'белый', 15, 2)

tape_white = Accessory('ленточка', 'белая ленточка для украшения букета', 2)
tape_red = Accessory('ленточка', 'красная ленточка для украшения букета', 2)

bouquet_1 = Bouquet()

bouquet_1.add_flower(rose, tulip, chamomile)
bouquet_1.add_accessory(tape_white, tape_red, tape_white)

bouquet_1.total_cost()
bouquet_1.time_wilting()

bouquet_1.sort_by_parameter("cost")
bouquet_1.sort_by_parameter("wilting")
bouquet_1.sort_by_parameter("length")

bouquet_1.find_by_parameter("cost", 4)
bouquet_1.find_by_parameter("weight", 1)

bouquet_1.if_flower_in('роза')
bouquet_1.if_flower_in('Пион')
