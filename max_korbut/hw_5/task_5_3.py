"""FuzzBuzz"""
"""Создадим список с числами в диапазоне от 1 до 100 включительно"""
numbers = []
for i in range(1, 101):
    numbers.append(i)
"""
Если отстаток от деления и на 3 и на 5 равен 0, то выводится "FuzzBuzz";
если только на 3,то "Fuzz";
если только на 5, то "Buzz"
"""
for number in numbers:
    if number % 3 == 0 and number % 5 == 0:
        print("FuzzBuzz")
    elif number % 3 == 0:
        print("Fuzz")
    elif number % 5 == 0:
        print("Buzz")
    else:
        print(number)
