from pages.contact_us_page import ContactUsPage


def test_contact_us(browser, get_data_for_contact_us):
    expected_message = "Your message has been successfully sent to our team."
    sub_heading, email, order_reference, message = get_data_for_contact_us
    contact_us_page = ContactUsPage(browser)
    contact_us_page.open_contact_us_page()
    contact_us_page.should_be_contact_us_page()
    contact_us_page.fill_in_form(sub_heading, email, order_reference, message)
    success_message = contact_us_page.get_success_fill_in_message()
    assert success_message.text == expected_message
