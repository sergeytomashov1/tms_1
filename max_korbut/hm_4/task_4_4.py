"""
Создать список из 10 элементов, на 3 позицию новое значение, 6й элемент удалить
"""
list = ['My', 'name', 'is', 'Max', 'and',
        'I', 'am', 'nineteen', 'years', 'old']
print(f"Список до изменения: {list}")
list.insert(3, '-')
del list[6]
print(f"Список после изменения: {list}")
