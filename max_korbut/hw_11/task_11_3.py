import json
from collections import Counter


def convert_json(way_json):
    """конвертирует json файл"""

    with open(way_json) as f:
        python_obj = json.load(f)
        return python_obj


def create_list(info, parameter):
    """Создание списка значений по нужному параметру"""

    new_list = []
    for student in info:
        new_list.append(student[parameter.title()])
    return new_list


def find_by_class_club(info):
    """поиск студентов в одном классе и клубе"""

    class_list = create_list(info, 'class')
    club_list = create_list(info, 'club')
    count_class = Counter(class_list)
    count_club = Counter(club_list)
    for key_class, value_class in count_class.items():
        if value_class > 1:
            for key_club, value_club in count_club.items():
                if value_club > 1:
                    i = 0
                    names = []
                    for student in info:
                        if student['Class'] == key_class and \
                                student['Club'] == key_club:
                            i += 1
                            names.append(student['Name'])
                    if i > 1:
                        print(f"В классе {key_class} на секцию {key_club} "
                              f"ходят: ")
                        for name in names:
                            print(f'\t{name}')


def filter_by_gender(info, gender):
    """фильтрация студентов по полу"""

    gender_list = []
    for student in info:
        if student['Gender'] == gender.upper():
            gender_list.append(student['Name'])
    print(f"По полу {gender} были найдены следующие студенты:")
    for name in gender_list:
        print(f"\t{name}")


def find_by_name(info, name):
    """поиск по имени или части имени"""

    for student in info:
        if name.title() in student['Name']:
            print(f"По имени {name} был найден студент с ID {student['ID']}")


if __name__ == "__main__":
    students_info = convert_json('students.json')

    find_by_class_club(students_info)

    filter_by_gender(students_info, 'W')
    filter_by_gender(students_info, 'm')

    find_by_name(students_info, 'sEnpai')
    find_by_name(students_info, 'Rio')
    find_by_name(students_info, 'Koharu Hinata')
