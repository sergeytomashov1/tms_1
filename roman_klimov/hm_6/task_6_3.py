print('КАЛЬКУЛЯТОР')
print('1. Сложение\n'
      '2. Вычитание\n'
      '3. Умножение\n'
      '4. Деление')
a = input('Выберете операцию:')
x = float(input('x = '))
y = float(input('y = '))
if a == '1':
    print(x + y)
elif a == '2':
    print(x - y)
elif a == '3':
    print(x * y)
elif a == '4':
    if y != 0:
        print(x / y)
    else:
        print('Деление на ноль!')
else:
    print('Такая операция не реализована в v0.1')
