from pages.login_page import LoginPage
from pages.main_page import MainPage
from pages.shirts_page import ShirtsPage
from pages.cart_page import CartPage


# Проверка возможности добавления твоара в корзину
def test_empty_cart(browser):
    main_page = MainPage(browser)
    main_page.open_base_page()
    main_page.open_login_page()
    login_page = LoginPage(browser)
    login_page.login('test_for_ex@gmail.com', '11111')
    shirts_pages = ShirtsPage(browser)
    shirts_pages.open_login_page()
    shirts_pages.add_product_shirts()
    checking_cart = CartPage(browser)
    checking_cart.open_cart_page()
    checking_cart.checking_product_in_cart()
