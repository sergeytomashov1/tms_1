def return_max(ls: list):
    result = []
    for i in range(1, len(ls)):
        if ls[i] > ls[i - 1]:
            result.append(ls[i])
    return result


print(return_max([1, 5, 2, 10, 4, 7]))
