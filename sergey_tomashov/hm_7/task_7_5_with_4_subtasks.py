import random as r


def decode(letters: str, step=1, lang='en', decode='Yes'):
    """

    :param letters: string that You need to decode
    :param step: step for decode algorithm
    :param lang: 'ru', 'en' fro corresponding chars
    :param decode: 'Yes' - decode, 'No' - reverse
    :return:
    """
    abc = 'abcdefghijklmnopqrstuvwxyz'
    abc_ru = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'
    abc_len = len(abc)
    decode_string = ''
    if decode == 'Yes':
        if lang == 'en':
            for i in letters:
                if i in abc.upper():
                    index = abc.upper().index(i) + step
                    decode_string += abc[index % abc_len].upper()
                elif i in abc.lower():
                    index = abc.index(i) + step
                    decode_string += abc[index % abc_len]
                else:
                    decode_string += i
        if lang == 'ru':
            abc_len = len(abc_ru)
            for i in letters:
                if i in abc_ru.upper():
                    index = abc_ru.upper().index(i) + step
                    decode_string += abc_ru[index % abc_len].upper()
                elif i in abc_ru.lower():
                    index = abc_ru.index(i) + step
                    decode_string += abc_ru[index % abc_len]
                else:
                    decode_string += i

    elif decode == 'No':
        if lang == 'en':
            for i in letters:
                if i in abc.upper():
                    index = abc.upper().index(i) - step
                    decode_string += abc[index % abc_len].upper()
                elif i in abc.lower():
                    index = abc.index(i) - step
                    decode_string += abc[index % abc_len]
                else:
                    decode_string += i
        if lang == 'ru':
            abc_len = len(abc_ru)
            for i in letters:
                if i in abc_ru.upper():
                    index = abc_ru.upper().index(i) - step
                    decode_string += abc_ru[index % abc_len].upper()
                elif i in abc_ru.lower():
                    index = abc_ru.index(i) - step
                    decode_string += abc_ru[index % abc_len]
                else:
                    decode_string += i
    return decode_string


print(decode.__doc__)
print(decode('ееж', 5, 'ru', 'No'))


def my_own_decode(letters):
    abc = 'abcdefghijklmnopqrstuvwxyz'
    digits = '1234567890'
    hashes = []
    for i in range(0, len(letters)):
        if i % 2 == 0:
            lett = letters[i] + r.choice(abc) + r.choice(digits)
            hashes.append(lett)
        else:
            lett = r.choice(abc) + r.choice(digits) + letters[i]
            hashes.append(lett)
    return ''.join(hashes)


print(my_own_decode('abcde'))
