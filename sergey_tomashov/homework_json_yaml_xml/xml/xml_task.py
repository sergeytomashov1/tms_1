import xml.etree.ElementTree as ET


class XMLLib:
    def __init__(self, file):
        self.tree = ET.parse(file).getroot()

    def search_author(self, name):
        for child in self.tree:
            if name in child[0].text:
                for i in child:
                    print(i.text)

    def search_price(self, price):
        for child in self.tree:
            if price > float(child[3].text):
                for i in child:
                    print(i.text)

    def search_param(self, param, value):
        for child in self.tree:
            for i in child:
                if (i.tag == param) and (value in i.text):
                    print(f'[{value}] found '
                          f'in {child[1].text} book, in:\n {i.text}')


lib1 = XMLLib('library.xml')

print('____')
lib1.search_param('title', 'Midnight')
