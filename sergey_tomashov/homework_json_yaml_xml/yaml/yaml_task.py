import yaml
import json

with open('yaml/order.yaml', 'r') as f:
    data = yaml.safe_load(f)

print(data['invoice'])
print(data['bill-to']['address'])
for i in data['product']:
    print(i['description'])
    print(i['quantity'])
    print(i['price'])

with open('yaml/order.yaml', 'r') as yml, \
        open('yaml/yaml_to_json.json', 'w') as jsn:
    yaml_file = yaml.safe_load(yml)
    json.dump(yaml_file, jsn)
