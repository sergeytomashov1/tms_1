import pytest
from hm_pytest.cows import cows
import random


class TestCows():
    invalid = 'input should be 4 non repeatable digits'
    repeat = 'Repeatable digits in input'

    @pytest.fixture()
    def set_up(self):
        self.number = ''.join(random.sample('1234567890', 4))
        self.bull = "{0}{1}{2}".format(self.number[3],
                                       self.number[1:3],
                                       self.number[0])
        return self.number, self.bull

    @pytest.fixture(params=['12', '  ', 'abcd', '%ˆˆ$'])
    def invalid_data(self, request):
        return request.param

    @pytest.mark.regression
    def test_invalid_data(self, invalid_data):
        assert cows('1234', invalid_data) == \
               f'{invalid_data} {self.invalid}'

    @pytest.mark.smoke
    def test_won(self, set_up):
        assert cows(self.number, self.number) == \
               f'Congrats! You {self.number} ' \
               f'same as {self.number}'

    @pytest.mark.regression
    def test_repeat(self, set_up):
        assert cows(self.number, '2222') == self.repeat

    @pytest.mark.smoke
    def test_cows(self, set_up):
        assert cows(self.number, self.number[::-1]) == \
               'Cows :4, Bulls: 0'

    @pytest.mark.smoke
    def test_bulls(self, set_up):
        assert cows(self.number, self.bull) == \
               'Cows :2, Bulls: 2'
