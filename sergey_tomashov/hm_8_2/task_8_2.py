"""
Задание 2
Напишите функцию декоратор, которая будет измерять
время работы декорируемой функции
"""
from time import time, sleep


def time_count(func):
    def wrapper():
        start_tine = time()
        func()
        result_time = time() - start_tine
        print(f'Время выполнения функции {result_time}')

    return wrapper


@time_count
def counter():
    sleep(5)
    print('end')


counter()
