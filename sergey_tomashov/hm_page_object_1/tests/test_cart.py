from sergey_tomashov.hm_page_object_1.pages.base_page import BasePage
from sergey_tomashov.hm_page_object_1.pages.cart_page import CartPage
from sergey_tomashov.hm_page_object_1.pages.toolbar_element \
    import ToolbarElement
from sergey_tomashov.hm_page_object_1.pages.general_methods \
    import GeneralMethods


def test_empty_cart(browser):
    cart_page = CartPage(browser)
    cart_page.open_base_page()
    cart_page.open_cart_page()
    cart_page.check_empty_cart()


def test_add_to_card(browser):
    base_page = BasePage(browser)
    base_page.open_base_page()
    toolbar = ToolbarElement(browser)
    toolbar.open_random()
    gm = GeneralMethods(browser)
    rand_product = gm.random_product()
    rand_product.click()
    gm.add_product_to_cart()
    product_title = gm.get_product_title_from_quick_preview().text
    gm.close_quick_preview()
    cart = CartPage(browser)
    cart.open_cart_page()
    cart.check_cart_not_empty()
    cart.find_products_in_cart(product_title)
