from sergey_tomashov.hm_page_object_1.pages.base_page \
    import BasePage
from sergey_tomashov.hm_page_object_1.locators.toolbar_locators \
    import ToolBarLocators
import random


class ToolbarElement(BasePage, ToolBarLocators):

    def open_women(self):
        women_link = self.find_element(self.LOCATOR_WOMEN)
        women_link.click()

    def open_dresses(self):
        dress_link = self.find_element(self.LOCATOR_DRESS)
        dress_link.click()

    def open_tshirt(self):
        tshirt_link = self.find_element(self.LOCATOR_SHIRT)
        tshirt_link.click()

    def open_random(self):
        elements = self.find_elements(self.LOCATOR_TOOLBAR_ELEMENT)
        random.choice(elements).click()
