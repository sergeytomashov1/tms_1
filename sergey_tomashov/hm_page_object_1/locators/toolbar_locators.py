from selenium.webdriver.common.by import By


class ToolBarLocators:
    LOCATOR_WOMEN = (By.XPATH, "//a[@title='Women']")
    LOCATOR_DRESS = (By.XPATH, "//a[@title='Dresses']")
    LOCATOR_SHIRT = (By.XPATH, "//a[@title='T-shirts']")
    LOCATOR_TOOLBAR_ELEMENT = (
        By.XPATH, '//ul[@class="sf-menu clearfix menu-content '
                  'sf-js-enabled sf-arrows"]/li')
