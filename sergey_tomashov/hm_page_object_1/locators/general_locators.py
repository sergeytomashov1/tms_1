from selenium.webdriver.common.by import By


class GeneralLocators:
    LOCATOR_PRODUCT = (By.XPATH, '//div[@class="product-image-container"]')
    LOCATOR_ADD_TO_CART = (By.XPATH, '//p[@id="add_to_cart"]/button')
    LOCATOR_PRODUCT_FRAME = (By.XPATH, "//iframe")
    LOCATOR_ADDED_CART_TEXT = (
        By.XPATH, '//div[@class="layer_cart_product col-xs-12 col-md-6"]/h2')
    LOCATOR_CLOSE_QUICK_PREVIEW = (By.XPATH, '//span[@class="cross"]')
    LOCATOR_PRODUCT_TITLE = (By.ID, 'layer_cart_product_title')
