from selenium.webdriver.common.by import By


class CartLOcators:
    LOCATOR_CART = (By.ID, 'cart_title')
    LOCATOR_CART_LINK = (By.XPATH, "//a[@title='View my shopping cart']")
    LOCATOR_CART_EMPTY_TEXT = (By.XPATH, '//p[@class="alert alert-warning"]')
    LOCATOR_CART_WITH_PRODUCT = (By.XPATH, '//table[@id="cart_summary"]')
    LOCATOR_PRODUCT_NAME = (
        By.XPATH, "//td[@class='cart_description']/p[@class='product-name']")
