def decorator(func):
    def wrapper(row):
        number_names = {
            0: 'zero', 1: 'one', 2: 'two', 3: 'three',
            4: 'four', 5: 'five', 6: 'six', 7: 'seven',
            8: 'eight', 9: 'nine', 10: 'ten',
            11: 'eleven', 12: 'twelve', 13: 'thirteen',
            14: 'fourteen', 15: 'fifteen', 16: 'sixteen',
            17: 'seventeen', 18: 'eighteen', 19: 'nineteen'}

        new_row = [int(i) for i in row.split()]
        new_dict = {v: k for (k, v) in number_names.items() if k in new_row}

        sorted_keys = sorted(new_dict)

        ls = [str((new_dict[key])) for key in sorted_keys]
        string_out = ', '.join(ls)
        return func(string_out)

    return wrapper


@decorator
def abc_sort(row):
    print(row)


abc_sort('1 2 3')
abc_sort('19 15 11')
