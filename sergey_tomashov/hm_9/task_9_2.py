class Investing:
    def __init__(self, summa, period):
        self.summa = summa
        self.period = period

    @property
    def investing(self):
        return self.summa

    @investing.setter
    def investing(self, value):
        self.summa += value

    @investing.deleter
    def investing(self):
        self.summa = 0


class Bank:

    def __init__(self, name):
        self.name = name
        self.deposits = []

    def deposit_info(self, investing):
        summ = investing.summa
        for i in range(0, investing.period):
            summ += summ * 0.10 / 12
        print(f'Положив {investing.summa} в банк {self.name} '
              f'на {investing.period} месяцев, вы получите {summ}')
        return summ


prior = Bank('Prior')
inv = Investing(10, 10)
inv.investing = 20
del inv.investing
inv.investing = 10
print(inv.investing)

print(prior.deposit_info(inv))
