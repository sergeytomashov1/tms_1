from selenium.webdriver.common.by import By


def test_click_on_button(browser):
    browser.get('https://ultimateqa.com/complicated-page/')
    b_xpath = browser.find_element(
        By.XPATH, '//a[@class="et_pb_button '
                  'et_pb_button_4 et_pb_bg_layout_light"]')
    b_xpath.click()

    b_css = browser.find_element(By.CLASS_NAME, 'et_pb_button_4')
    b_css.click()

    b_class_name = browser.find_element(
        By.CSS_SELECTOR, '[class="et_pb_button '
                         'et_pb_button_4 et_pb_bg_layout_light"]')
    b_class_name.click()
