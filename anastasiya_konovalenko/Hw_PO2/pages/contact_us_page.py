from pages.base_page import BasePage
from locator.contact_page_locator import ContactPageLocator
from selenium.webdriver.support.ui import Select


class ContactUsPage(BasePage):

    def should_be_contact_page(self):
        self.contact_form_is_present()

    def contact_form_is_present(self):
        contact_text = self.find_element(
            ContactPageLocator.LOCATOR_CONTACT_TEXT).text
        check_text = 'SEND A MESSAGE'
        assert contact_text == check_text

    def message_successfully_sent(self):
        message_text = self.find_element(
            ContactPageLocator.LOCATOR_SUCCESS_SENT).text
        check_text = 'Your message has been successfully sent to our team.'
        assert message_text == check_text

    def contact(self):
        select_index = Select(self.find_element(
            ContactPageLocator.LOCATOR_SUBJECT_HEADING))
        select_index.select_by_index(2)
        email_field = self.find_element(ContactPageLocator.LOCATOR_EMAIL_FIELD)
        email_field.send_keys('dmitry@gmail.com')
        message_field = self.find_element(
            ContactPageLocator.LOCATOR_MESSAGE_FORM)
        message_field.send_keys('Please help with ordering!!')
        order_reference_field = self.find_element(
            ContactPageLocator.LOCATOR_ORDER_REFERENCE_FORM)
        order_reference_field.send_keys('1234567')
        button_submit = self.find_element(
            ContactPageLocator.LOCATOR_BUTTON_SUBMIT)
        button_submit.click()
