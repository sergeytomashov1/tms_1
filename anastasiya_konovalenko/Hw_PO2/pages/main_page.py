from pages.base_page import BasePage
from locator.main_page_locator import MainPageLocator
from locator.basket_page_locator import BasketPageLocator
from locator.contact_page_locator import ContactPageLocator


class MainPage(BasePage):

    def open_login_page(self):
        sign_in_button = self.find_element(
            MainPageLocator.LOCATOR_SIGN_IN_BUTTON
        )
        sign_in_button.click()

    def open_basket_page(self):
        basket_button_click = self.find_element(
            BasketPageLocator.LOCATOR_BUTTON_BASKET)
        basket_button_click.click()

    def open_contact_page(self):
        contact_button_click = self.find_element(
            ContactPageLocator.LOCATOR_BUTTON_CONTACT_CLICK)
        contact_button_click.click()
