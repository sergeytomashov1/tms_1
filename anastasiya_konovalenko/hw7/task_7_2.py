import numpy


onedimensional_array = [1, 2, 3, 4, 5, 6, 7, 8]


def array(onedimensional_array, str, column):
    arr = numpy.array(onedimensional_array)
    twodimensional_array = arr.reshape(str, column)
    return twodimensional_array


print(array(onedimensional_array, 4, 2))
