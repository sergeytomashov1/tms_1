from pages.base_page import BasePage
from locator.catalog_women_locator import CatalogWomenPageLocator


class CatalogWomenPage(BasePage):

    def product_search(self):
        img_product = self.find_element(
            CatalogWomenPageLocator.LOCATOR_PRODUCT_SEARCH)
        img_product.click()
