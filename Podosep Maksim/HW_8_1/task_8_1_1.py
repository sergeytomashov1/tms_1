from time import time, sleep


def my_decorator(func):
    def wrapper(t_s):
        start = time()
        func(t_s)
        sleep(t_s)
        end = time()
        print(round((end - start), 2))
    return wrapper


@my_decorator
def tim(n):
    return n


tim(5)
