from time import time, sleep


def caching(timeout):
    a = {'x': 0, 'value': 0, 'cache_time': 0}

    def wrap(func_1):
        def wrapper(y):
            b = func_1(y)
            t: float = time()
            if a['x'] == y:
                if (t - a['cache_time']) > timeout:
                    a['x'] = y
                    a['value'] = b[y]
                    a['cache_time'] = t
                    print(a)
                else:
                    print()
            else:
                a['x'] = y
                a['value'] = b[y]
                a['cache_time'] = t
                print(a)
        return wrapper
    return wrap


@caching(timeout=3)
def func(z):
    return {z: 42}


func(5)
sleep(2)
func(5)
func(3)
