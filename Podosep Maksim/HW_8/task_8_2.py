number_names = {0: 'zero', 1: 'one', 2: 'two', 3: 'three',
                4: 'four', 5: 'five', 6: 'six', 7: 'seven',
                8: 'eight', 9: 'nine', 10: 'ten', 11: 'eleven',
                12: 'twelve', 13: 'thirteen', 14: 'fourteen',
                15: 'fifteen', 16: 'sixteen',
                17: 'seventeen', 18: 'eighteen', 19: 'nineteen'}


def look_for(func):
    def wrapper(string):
        y = func(string)
        ss = []
        for i_s in y:
            for k, v in number_names.items():
                if i_s == k:
                    ss.append(v)
        ss.sort()
        print(' '.join(ss))
    return wrapper


@look_for
def enter_numbers(string_1):
    s = []
    b = string_1.split()
    x = 0
    for i in b:
        if i != ' ':
            s.append(int(i))
    for ii in s:
        if ii < 0 or ii > 19:
            x += 1
            print('enter the correct numbers')
            return()
    if x == 0:
        return s


enter_numbers('10 12 15')
