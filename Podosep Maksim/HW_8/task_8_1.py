def my_decorator(type_1):
    def inner(func):
        def wrapper(a, b, c=None):
            if type_1 == 'str':
                a = str(a)
                b = str(b)
                if c is None:
                    c = ''
                else:
                    c = str(c)
                func(a, b, c)
            else:
                s = []
                ss = []
                s.append(a)
                s.append(b)
                if c is not None:
                    s.append(c)

                for i in s:
                    if isinstance(i, (float, int)):
                        ss.append(i)
                    else:
                        ii = int(i)
                        ss.append(ii)

                a = ss[0]
                b = ss[1]
                if c is not None:
                    c = ss[2]
                else:
                    c = 0
                func(a, b, c)
        return wrapper
    return inner


@my_decorator('int')
def add_d(a, b, c=None):
    print(a + b + c)


add_d('3', 5)
