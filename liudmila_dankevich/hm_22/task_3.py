# 3)	работа с json файлом
# Разработайте поиск учащихся в одном классе, посещающих одну секцию.
# Фильтрацию учащихся по их полу. Поиск ученика по имени(часть имени)


import json

data = [
    {
        "ID": "1",
        "Name": "Senpai",
        "Gender": "M",
        "Class": "1b",
        "Club": "Football",
        "Strength": "5",
        "Hairstyle": "1",
        "Color": "Black"
    },
    {
        "ID": "2",
        "Name": "Yui Rio",
        "Gender": "W",
        "Class": "3a",
        "Club": "Chess",
        "Strength": "3",
        "Hairstyle": "2",
        "Color": "Red"
    },
    {
        "ID": "3",
        "Name": "Yuna Hina",
        "Gender": "W",
        "Class": "5a",
        "Club": "Football",
        "Strength": "6",
        "Hairstyle": "3",
        "Color": "Yellow"
    },
    {
        "ID": "4",
        "Name": "Koharu Hinata",
        "Gender": "W",
        "Class": "5a",
        "Club": "Chess",
        "Strength": "4",
        "Hairstyle": "4",
        "Color": "Green"
    },
    {
        "ID": "5",
        "Name": "Mei Mio",
        "Gender": "W",
        "Class": "1b",
        "Club": "box",
        "Strength": "7",
        "Hairstyle": "5",
        "Color": "Blue"
    },
    {
        "ID": "6",
        "Name": "Saki Miyu",
        "Gender": "W",
        "Class": "1a",
        "Club": "box",
        "Strength": "7",
        "Hairstyle": "6",
        "Color": "Cyan"
    },
    {
        "ID": "7",
        "Name": "Kokona Haruka",
        "Gender": "W",
        "Class": "3a",
        "Club": "box",
        "Strength": "8",
        "Hairstyle": "7",
        "Color": "Purple"
    },
    {
        "ID": "8",
        "Name": "Haruto Yuto",
        "Gender": "M",
        "Class": "3a",
        "Club": "Chess",
        "Strength": "2",
        "Hairstyle": "2",
        "Color": "Red"
    },
    {
        "ID": "9",
        "Name": "Sota Yuki",
        "Gender": "M",
        "Class": "3b",
        "Club": "Football",
        "Strength": "4",
        "Hairstyle": "3",
        "Color": "Yellow"
    },
    {
        "ID": "10",
        "Name": "Hayato Haruki",
        "Gender": "M",
        "Class": "5a",
        "Club": "Football",
        "Strength": "5",
        "Hairstyle": "4",
        "Color": "Green"
    }
]

data_str = json.dumps(data)


def find_class(data):
    for item in data:
        if item['Class'] == '5a':
            print(item['Class'], item['Name'])


def find_gender(data):
    for item in data:
        if item['Gender'] == 'M':
            print(item['Gender'], item['Name'])


def find_club(data):
    for item in data:
        if item['Club'] == 'Football':
            print(item['Club'], item['Name'])


def find_name(data):
    for item in data:
        if item['Name'] == 'Mei Mio':
            print(item['Name'], item['Club'], item['Gender'], item['Class'],
                  item['Strength'], item['Hairstyle'], item['Color'])


find_class(data)
find_gender(data)
find_club(data)
find_name(data)
