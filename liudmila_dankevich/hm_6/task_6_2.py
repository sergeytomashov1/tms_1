# Подсчет количества букв
# На вход подается строка, например, "cccbba" результат
# работы программы - строка “c3b2a"

# Примеры для проверки работоспособности:
# "cccbba" == "c3b2a"
# "abeehhhhhccced" == "abe2h5c3ed"
# "aaabbceedd" == "a3b2ce2d2"
# "abcde" == "abcde"
# "aaabbdefffff" == "a3b2def5"

from collections import Counter


def string_counter(str_1):
    str_1 = Counter(str_1)
    count_str_1 = ''
    for k, v in str_1.items():
        count_str_1 += k
        if v > 1:
            count_str_1 += str(v)
    return count_str_1


print(string_counter('aaabbbccc'))
print(string_counter('abeehhhhhccced'))
print(string_counter('aaabbceedd'))
print(string_counter('aaabbdefffff'))
print(string_counter('abcdr'))
