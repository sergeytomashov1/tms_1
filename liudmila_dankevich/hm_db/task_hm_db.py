# Таблица orders
#
# ord_no | purch_amt | ord_date | customer_id | salesman_id
# 70001 | 150.5 | 2012-10-05 | 3005 | 5002
# 70009 | 270.65 | 2012-09-10 | 3001 | 5005
# 70002 | 65.26 | 2012-10-05 | 3002 | 5001
# 70004 | 110.5 | 2012-08-17 | 3009 |5003
# 70007 | 948.5 | 2012-09-10 | 3005 | 5002
# 70005 | 2400.6 | 2012-07-27 | 3007 | 5001
# 70008 | 5760 | 2012-09-10 | 3002 | 5001
# 70010 | 1983.43 | 2012-10-10 | 3004 | 5006
# 70003 | 2480.4 | 2012-10-10 | 3009 | 5003
# 70012 | 250.45 | 2012-06-27 | 3008 | 5002
# 70011 | 75.29 | 2012-08-17 | 3003 | 5007
# 70013 | 3045.6 | 2012-04-25 | 3002 | 5001
#
# Заполнить таблицу данными, минимум 10 записей
#
# Напишите следующие sql запросы:
# 1)	Напечатайте номер заказа, дату заказа и количество
# для каждого заказа, Который продал продавец под номером: 5002
# 2)	Напечатайте уникальные id продавца(salesman_id).
# Используйте distinct
# 3)	Напечатайте по порядку данные о дате заказа,
# id продавца, номер заказа, количество
# 4)	Напечатайте заказы между 70001 и 70007
# (используйте between, and)

import mysql.connector as mysql

db = mysql.connect(
    host="localhost", user="root", passwd="1111",
    database="test_db_1")
cursor = db.cursor()

# cursor.execute("SHOW DATABASES")
# print(cursor.fetchall())

# создание таблицы
# query = "CREATE TABLE orders (ord_no VARCHAR(255)," \
#         "purch_amt VARCHAR(255)," \
#         "ord_date VARCHAR(255)," \
#         "customer_id VARCHAR(255)," \
#         "salesman_id VARCHAR(255))"
#
# cursor.execute(query)
#
# cursor.execute("SHOW TABLES")
# print(cursor.fetchall())


# # добавление информации в таблицу

# query = "INSERT INTO orders (ord_no, purch_amt,
# ord_date, customer_id, salesman_id)
# VALUES (%s, %s, %s, %s, %s)"
#
# values = [
#     ("70001", "150.5", "2012-10-05", "3005", "5002"),
#     ("70009", "270.65", "2012-09-10", "3001", "5005"),
#     ("70002", "65.26", "2012-10-05", "3002", "5001"),
#     ("70004", "110.5", "2012-08-17", "3009", "5003"),
#     ("70007", "948.5", "2012-09-10", "3005", "5002"),
#     ("70005", "2400.6", "2012-07-27", "3007", "5001"),
#     ("70008", "5760", "2012-09-10", "3002", "5001"),
#     ("70010", "1983.43", "2012-10-10", "3004", "5006"),
#     ("70003", "2480.4", "2012-10-10", "3009", "5003"),
#     ("70012", "250.45", "2012-06-27", "3008", "5002")
# ]
# cursor.executemany(query, values)
# db.commit()
# print(cursor.rowcount, "record inserted")
# cursor.execute("Select * FROM orders")
# print(cursor.fetchall())

# 1)Напечатайте номер заказа,
# дату заказа и количество для каждого заказа,
# Который продал продавец под номером: 5002

# query = 'SELECT ord_no, purch_amt,
# ord_date FROM orders WHERE salesman_id = 5002'
# cursor.execute(query)
# print(cursor.fetchall())


# 2)Напечатайте уникальные id продавца(salesman_id).
# Используйте distinct
# query = 'SELECT DISTINCT salesman_id FROM orders'
# cursor.execute(query)
# print(cursor.fetchall())


# 3)Напечатайте по порядку данные о дате заказа,
# id продавца, номер заказа, количество
# query = 'Select ord_date, customer_id, ord_no,
# purch_amt FROM orders'
# cursor.execute(query)
# print(cursor.fetchall())

# 4)Напечатайте заказы между 70001 и 70007(используйте between, and)

# query = "SELECT * FROM orders WHERE ord_no BETWEEN 70002 AND 70006"
# cursor.execute(query)
# print(cursor.fetchall())
