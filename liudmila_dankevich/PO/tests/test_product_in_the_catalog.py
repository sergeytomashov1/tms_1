from pages.main_page import MainPage
from pages.catalog_women import CatalogWomenPage
from pages.short_sleeve_t_shirts_page import ShortSleeveshirtsPage


def test_product_in_the_catalog(browser):
    # открываем главную страницу
    main_page = MainPage(browser)
    main_page.open_base_page()
    # открываем каталог женской одежды
    main_page.open_women_catalog_page()
    catalog_women_page = CatalogWomenPage(browser)
    # выбираем продукт
    catalog_women_page.product_search()
    short_sleeve_shirts = ShortSleeveshirtsPage(browser)
    short_sleeve_shirts.product_add_to_cart()
    # short_sleeve_shirts.proceed_to_checkout() #выдает ошибку
