# 1.	Библиотека
# Создайте класс book с именем книги, автором, кол-м страниц,
# ISBN, флагом, зарезервирована ли книги или нет. Создайте
# класс пользователь
# который может брать книгу, возвращать, бронировать. Если
# другой пользователь
# хочет взять зарезервированную книгу(или которую уже
# кто-то читает - надо ему
# про это сказать).

class Book:
    """Создаем атрибут книги, True - книга доступна,
     False - книга занята"""

    def __init__(self, book_name, author, pages, isbn, flag):
        self._book_name = book_name
        self._author = author
        self._pages = pages
        self._isbn = isbn
        self.flag = flag

    def information_about_book(self):
        """Показывает данные книги"""

        information_about_book = f"{self._book_name}, " \
                                 f"автор {self._author}," \
                                 f" {self._pages}, " \
                                 f"{self._isbn}"
        return information_about_book

    def check_book(self):
        """Проверка наличия книги"""

        return self.flag

    def take_book(self):
        """Книгу взяли"""

        self.flag = False
        return self.flag

    def return_book(self):
        """Книгу вернули"""

        self.flag = True
        return self.flag


class User:
    """Создание пользователя"""

    def __init__(self, user_name):
        self.user_name = user_name

    def reserve_book(self, book):
        """Бронирование книги"""
        print(f"{self.user_name}, Вы хотите забронировать книгу"
              f" {book.information_about_book()},"
              f"сейчас проверим свободна ли она.")
        print(f"{self.user_name} книга свободна."
              f" Mы забронировали за вами книгу"
              f" {book.information_about_book()}"
              f" бронь действительна в течении 24 часов")
        book.take_book()
        print("К сожалению книга занята, выберите другую")

    def give_book_to_user(self, book):
        """Выдает книгу пользователю"""

        print(f"{self.user_name}, Вы выбрали книгу "
              f"{book.information_about_book()},"
              f"сейчас проверим свободна ли она.")
        print(f"{self.user_name}, книга свободна."
              f" Mы записываем на вас книгу"
              f" {book.information_about_book()}")
        book.take_book()
        print("К сожалению книга занята, "
              "выберите другую")

    def return_book_to_library(self, book):
        """Возвращает книгу в библиотеку"""

        print(f"{self.user_name} Вы хотите вернуть книгу "
              f"{book.information_about_book()}")
        book.return_book()
        print(f"{self.user_name} вернули книгу "
              f"{book.information_about_book()}")


book_1 = Book('Русские сказки о животных', "Толстой Л.Н.",
              303, 9785699132966, True)
book_2 = Book('Самолетик Тема', "Агинская Е.Н.",
              32, 9785699132966, True)

user_1 = User("Olga")
user_2 = User("Sergey")

user_1.reserve_book(book_1)
user_1.give_book_to_user(book_2)
user_2.give_book_to_user(book_1)
user_1.return_book_to_library(book_2)
user_1.return_book_to_library(book_1)
user_2.return_book_to_library(book_1)
user_1.reserve_book(book_2)
