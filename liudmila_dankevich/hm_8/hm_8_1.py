"""Напишите декоратор, который проверял бы
тип параметров функции,
конвертировал их если надо и складывал:

@typed(type=’str’)
def add(a, b):
    return a + b

add("3", 5) -> "35"
add(5, 5) -> "55"
add('a', 'b') -> 'ab’

@typed(type=’int’)
def add(a, b, с):
    return a + b + с

add(5, 6, 7) -> 18
add("3", 5) -> 8
add(0.1, 0.2, 0.4) -> 0.7000000000000001
"""


def typed(type):
    def dekor(func):
        def wrapper(*args, ):
            res = []
            for i in args:
                res.append(type(i))
            return func(*res)

        return wrapper

    return dekor


@typed(type=str)
def add(a, b):
    return a + b


print(add("3", 5))
print(add(5, 5))
print(add('a', 'b'))


@typed(type=int)
def add_1(a, b, c):
    return a + b + c


print(add_1(5, 6, 7))
print(add_1("3", 9, 5))


@typed(type=float)
def add_2(a, b, c):
    return a + b + c


print(add_2(0.1, 0.2, 0.4))
