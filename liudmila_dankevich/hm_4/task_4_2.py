# Дан список: [‘Ivan’, ‘Ivanov’], и 2 строки: Minsk, Belarus
# Напечатайте текст: “Привет, Ivan Ivanov!
# Добро пожаловать в Minsk Belarus”


a = ['Ivan', 'Ivanov']
b = 'Minsk'
c = 'Belarus'
print("Привет, " + (' '.join(a)) + f"! Добро пожаловать в {b} {c}")
